$( document ).ready(function() {
    M.AutoInit();
    cargarPusheen();
});

const socket = io();
var list = document.querySelector('#lista-users');
const username = window.location.pathname.replace('/chat/','');
var clientes = [];

document.querySelector('#input').addEventListener('keypress', function (e) {
    var key = e.which || e.keyCode;
    if (key === 13) { // 13 is enter
    	enviarMensaje(e,1);
    }
});

document.querySelector('#div-pusheen').addEventListener('click',(e)=>{
	if(e.target.className === 'pusheen'){	
        let img = `${e.target.src}`;
       	enviarMensaje(e,2,img);
       	let elem = document.querySelector('.modal');
       	let instancia = M.Modal.getInstance(elem);
       	instancia.close();
    }
});



let scroll  = 0;

function scroll_to(div){
	if (div.scrollTop < div.scrollHeight - div.clientHeight) {
		div.scrollTop += 1000; //Se mueve hacia abajo
	}
}

document.querySelector('#boton').addEventListener('click', conectarChat);

function conectarChat(){
	//Identificador del socket
	let id = socket.id;
	console.log(`id: ${socket.id} username: ${username}`);
	$.post('/login', {
		username: username,
		id: id
	}, data => {
		console.log(data);
		clientes = data;
		list.innerHTML += 'Cargando...';
		let html = '';
		clientes.forEach(cliente => {
			html += `<li>${cliente.username}</li>`;
		});
		list.innerHTML = html;
		$('.loader').fadeOut();

	});
}

function enviarMensaje(e,tipo,img = 0){
	console.log(tipo);
	//Evitar marquee
	if(tipo == 1){
		if(e.which != 13) return;
		let msg = document.querySelector('#input').value;
		if (msg === "") return;
		$.post('/send', {
			text: msg,
			username: username,
			id: socket.id,
			tipo: 1
		}, data =>{
			document.querySelector("#input").value;
		});

		document.querySelector("#input").value = "";
	} else if(tipo == 2){
		console.log(img);
		$.post('/send', {
			text: img,
			username: username,
			id: socket.id,
			tipo: 2
		}, data =>{
			document.querySelector("#input").value;
		});

	}
	
}

socket.on('mensaje',data => {
	data.username = data.username.replace(/</g,'');
	data.username = data.username.replace('>/','');
	data.username = data.username.replace(/\//g,'');
	data.username = data.username.replace(/%20/g, " ");
	let sanitized  = data.msg.replace(/>/g,'');
	sanitized = sanitized.replace(/</g,'');
	console.log(':(');
	console.log(data.tipo);
	let clase = '';
	if(data.username == username) {
		clase = 'mio'; 
	}
	if (data.id == socket.id) {
		
		var msg = '';
		if(data.tipo == 1){
			sanitized = sanitized.replace(/\//g,'');
			msg = `
			<div class='local-message ${clase}'>
				<strong>
					${data.username}:
				</strong>
				<p>${sanitized}</p>
			</div>
		`;
		console.log('aqui');
		} else if(data.tipo == 2){
			console.log('Tipo 2');
			msg = `
			<div class='local-message ${clase}'>
				<strong>
					${data.username}:
				</strong>
				<p><img src="${sanitized}" width='200px'></p>
			</div>
		`;
		}
		document.querySelector('.mensajes-container').innerHTML += msg;
	} else {

		var msg = '';
		if(data.tipo == 1){
			sanitized = sanitized.replace(/\//g,'');
			msg = `
			<div class='local-message ${clase}'>
				<strong>
					${data.username}:
				</strong>
				<p>${sanitized}</p>
			</div>
		`;
		} else if(data.tipo == 2){
			console.log('Tipo 2');
			msg = `
			<div class='local-message ${clase}'>
				<strong>
					${data.username}:
				</strong>
				<p><img src="${sanitized}" width='200px'></p>
			</div>
		`;
		}
		document.querySelector('.mensajes-container').innerHTML += msg;
	}
	document.querySelector('#msg-container').scrollIntoView(false);
});


socket.on('socket_desconectado',data=>{
	console.log(data);
	clientes = clientes.filter(cliente => {
		console.log(cliente);
		return cliente.id != data.id;
	});
	list.innerHTML += 'Adios...';
	let html = '';
	clientes.forEach(cliente => {
		html += `<li>${cliente.username}</li>`;
	});
	list.innerHTML = html;
})

socket.on('socket_conectado',data=>{
	console.log(data);
	clientes.push(data);
	list.innerHTML += 'Cargando...';
	let html = '';
	clientes.forEach(cliente => {
		html += `<li>${cliente.username}</li>`;
	});
	list.innerHTML = html;
});


//Lo de los pusheen
function cargarPusheen(){
	let div = document.querySelector('#div-pusheen');
	console.log(div);
	for (let i = 1 ; i < 80; i++) {
		let img = document.createElement('img');
		img.src=`/img/stickers/pusheen/${i}.png`;
		img.style.width="60px"
		img.style.cursor="pointer";
		img.classList.add('pusheen');
		img.alt = i;
		div.appendChild(img);
	}
	console.log(div);
}

