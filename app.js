const express = require('express');
/* Convierte en json las peticiones de cadena */
const bodyParser = require('body-parser');
let app = require('express')();
let server = require('http').Server(app);
let io = require('socket.io')(server);

var clientes = [];

app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

server.listen(8080, () => console.log('Servidor iniciado en el puerto 8080'));

app.get('/', (req,res) => {
	res.sendFile(`${__dirname}/public/index.html`);
});

app.get('/chat/:username', (req,res) => {
	res.sendFile(`${__dirname}/public/chat.html`);
});

/* Pila de clientes */
app.post('/login', (req,res) => {
	let username = req.body.username;
	let id = req.body.id;
	clientes.push({
		id: id,
		username: username
	});

	io.emit('socket_conectado',{
		id: id,
		username: username
	});

	return res.json(clientes);
});

app.post('/send', (req,res) => {
	let username = req.body.username;
	let id = req.body.id;
	let tipo = req.body.tipo;
	let msg = req.body.text;

	io.emit('mensaje',{
		id: id,
		username: username,
		msg: msg,
		tipo: tipo
	});

	return res.json({text: 'Mensaje enviado'});
});

io.on('connection', socket => {
	console.log(`Socket conectado`, socket.id);
	socket.on('disconnect', () => {
		clientes = clientes.filter(cliente => cliente.id != socket.id);
		io.emit('socket_desconectado', {
			texto: `El Socket se desconecto`,
			id: socket.id
		});
	});
});